/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.pojos;


import java.io.Serializable;
import java.math.BigDecimal;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VBudget implements Serializable {

    private static final long serialVersionUID = 1L;

    private final BigDecimal budget;
    private final Integer    fkCamp;
    private final String     name;
    private final String     location;
    private final Double     year;

    public VBudget(VBudget value) {
        this.budget = value.budget;
        this.fkCamp = value.fkCamp;
        this.name = value.name;
        this.location = value.location;
        this.year = value.year;
    }

    public VBudget(
        BigDecimal budget,
        Integer    fkCamp,
        String     name,
        String     location,
        Double     year
    ) {
        this.budget = budget;
        this.fkCamp = fkCamp;
        this.name = name;
        this.location = location;
        this.year = year;
    }

    /**
     * Getter for <code>camp.v_budget.budget</code>.
     */
    public BigDecimal getBudget() {
        return this.budget;
    }

    /**
     * Getter for <code>camp.v_budget.fk_camp</code>.
     */
    public Integer getFkCamp() {
        return this.fkCamp;
    }

    /**
     * Getter for <code>camp.v_budget.name</code>.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for <code>camp.v_budget.location</code>.
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * Getter for <code>camp.v_budget.year</code>.
     */
    public Double getYear() {
        return this.year;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VBudget (");

        sb.append(budget);
        sb.append(", ").append(fkCamp);
        sb.append(", ").append(name);
        sb.append(", ").append(location);
        sb.append(", ").append(year);

        sb.append(")");
        return sb.toString();
    }
}
