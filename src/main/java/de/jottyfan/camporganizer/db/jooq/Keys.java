/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq;


import de.jottyfan.camporganizer.db.jooq.tables.TCamp;
import de.jottyfan.camporganizer.db.jooq.tables.TCampdocument;
import de.jottyfan.camporganizer.db.jooq.tables.TCampprofile;
import de.jottyfan.camporganizer.db.jooq.tables.TDocument;
import de.jottyfan.camporganizer.db.jooq.tables.TDocumentrole;
import de.jottyfan.camporganizer.db.jooq.tables.TLocation;
import de.jottyfan.camporganizer.db.jooq.tables.TPerson;
import de.jottyfan.camporganizer.db.jooq.tables.TPersondocument;
import de.jottyfan.camporganizer.db.jooq.tables.TProfile;
import de.jottyfan.camporganizer.db.jooq.tables.TProfilerole;
import de.jottyfan.camporganizer.db.jooq.tables.TRss;
import de.jottyfan.camporganizer.db.jooq.tables.TSales;
import de.jottyfan.camporganizer.db.jooq.tables.TSalescontent;
import de.jottyfan.camporganizer.db.jooq.tables.TSalescontenttype;
import de.jottyfan.camporganizer.db.jooq.tables.TSalesprofile;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampdocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TCampprofileRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TDocumentroleRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TLocationRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersonRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersondocumentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TProfileRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TProfileroleRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TRssRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalesRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalescontentRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalescontenttypeRecord;
import de.jottyfan.camporganizer.db.jooq.tables.records.TSalesprofileRecord;

import org.jooq.ForeignKey;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables in
 * camp.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<TCampRecord> T_CAMP_PKEY = Internal.createUniqueKey(TCamp.T_CAMP, DSL.name("t_camp_pkey"), new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final UniqueKey<TCampdocumentRecord> T_CAMPDOCUMENT_PKEY = Internal.createUniqueKey(TCampdocument.T_CAMPDOCUMENT, DSL.name("t_campdocument_pkey"), new TableField[] { TCampdocument.T_CAMPDOCUMENT.PK }, true);
    public static final UniqueKey<TCampdocumentRecord> UK_CAMPDOCUMENT = Internal.createUniqueKey(TCampdocument.T_CAMPDOCUMENT, DSL.name("uk_campdocument"), new TableField[] { TCampdocument.T_CAMPDOCUMENT.FK_CAMP, TCampdocument.T_CAMPDOCUMENT.FK_DOCUMENT }, true);
    public static final UniqueKey<TCampprofileRecord> T_CAMPPROFILE_FK_PROFILE_FK_CAMP_MODULE_KEY = Internal.createUniqueKey(TCampprofile.T_CAMPPROFILE, DSL.name("t_campprofile_fk_profile_fk_camp_module_key"), new TableField[] { TCampprofile.T_CAMPPROFILE.FK_PROFILE, TCampprofile.T_CAMPPROFILE.FK_CAMP, TCampprofile.T_CAMPPROFILE.MODULE }, true);
    public static final UniqueKey<TCampprofileRecord> T_CAMPPROFILE_PKEY = Internal.createUniqueKey(TCampprofile.T_CAMPPROFILE, DSL.name("t_campprofile_pkey"), new TableField[] { TCampprofile.T_CAMPPROFILE.PK }, true);
    public static final UniqueKey<TDocumentRecord> T_DOCUMENT_NAME_KEY = Internal.createUniqueKey(TDocument.T_DOCUMENT, DSL.name("t_document_name_key"), new TableField[] { TDocument.T_DOCUMENT.NAME }, true);
    public static final UniqueKey<TDocumentRecord> T_DOCUMENT_PKEY = Internal.createUniqueKey(TDocument.T_DOCUMENT, DSL.name("t_document_pkey"), new TableField[] { TDocument.T_DOCUMENT.PK }, true);
    public static final UniqueKey<TDocumentroleRecord> T_DOCUMENTROLE_FK_DOCUMENT_CAMPROLE_KEY = Internal.createUniqueKey(TDocumentrole.T_DOCUMENTROLE, DSL.name("t_documentrole_fk_document_camprole_key"), new TableField[] { TDocumentrole.T_DOCUMENTROLE.FK_DOCUMENT, TDocumentrole.T_DOCUMENTROLE.CAMPROLE }, true);
    public static final UniqueKey<TDocumentroleRecord> T_DOCUMENTROLE_PKEY = Internal.createUniqueKey(TDocumentrole.T_DOCUMENTROLE, DSL.name("t_documentrole_pkey"), new TableField[] { TDocumentrole.T_DOCUMENTROLE.PK }, true);
    public static final UniqueKey<TLocationRecord> T_LOCATION_PKEY = Internal.createUniqueKey(TLocation.T_LOCATION, DSL.name("t_location_pkey"), new TableField[] { TLocation.T_LOCATION.PK }, true);
    public static final UniqueKey<TPersonRecord> T_PERSON_PKEY = Internal.createUniqueKey(TPerson.T_PERSON, DSL.name("t_person_pkey"), new TableField[] { TPerson.T_PERSON.PK }, true);
    public static final UniqueKey<TPersonRecord> UK_PERSON = Internal.createUniqueKey(TPerson.T_PERSON, DSL.name("uk_person"), new TableField[] { TPerson.T_PERSON.FORENAME, TPerson.T_PERSON.SURNAME, TPerson.T_PERSON.BIRTHDATE, TPerson.T_PERSON.FK_CAMP }, true);
    public static final UniqueKey<TPersondocumentRecord> T_PERSONDOCUMENT_FK_PERSON_NAME_KEY = Internal.createUniqueKey(TPersondocument.T_PERSONDOCUMENT, DSL.name("t_persondocument_fk_person_name_key"), new TableField[] { TPersondocument.T_PERSONDOCUMENT.FK_PERSON, TPersondocument.T_PERSONDOCUMENT.NAME }, true);
    public static final UniqueKey<TPersondocumentRecord> T_PERSONDOCUMENT_PKEY = Internal.createUniqueKey(TPersondocument.T_PERSONDOCUMENT, DSL.name("t_persondocument_pkey"), new TableField[] { TPersondocument.T_PERSONDOCUMENT.PK }, true);
    public static final UniqueKey<TProfileRecord> T_PROFILE_PKEY = Internal.createUniqueKey(TProfile.T_PROFILE, DSL.name("t_profile_pkey"), new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final UniqueKey<TProfileRecord> T_PROFILE_USERNAME_KEY = Internal.createUniqueKey(TProfile.T_PROFILE, DSL.name("t_profile_username_key"), new TableField[] { TProfile.T_PROFILE.USERNAME }, true);
    public static final UniqueKey<TProfileRecord> T_PROFILE_UUID_KEY = Internal.createUniqueKey(TProfile.T_PROFILE, DSL.name("t_profile_uuid_key"), new TableField[] { TProfile.T_PROFILE.UUID }, true);
    public static final UniqueKey<TProfileroleRecord> T_PROFILEROLE_FK_PROFILE_ROLE_KEY = Internal.createUniqueKey(TProfilerole.T_PROFILEROLE, DSL.name("t_profilerole_fk_profile_role_key"), new TableField[] { TProfilerole.T_PROFILEROLE.FK_PROFILE, TProfilerole.T_PROFILEROLE.ROLE }, true);
    public static final UniqueKey<TRssRecord> T_RSS_PKEY = Internal.createUniqueKey(TRss.T_RSS, DSL.name("t_rss_pkey"), new TableField[] { TRss.T_RSS.PK }, true);
    public static final UniqueKey<TSalesRecord> T_SALES_PKEY = Internal.createUniqueKey(TSales.T_SALES, DSL.name("t_sales_pkey"), new TableField[] { TSales.T_SALES.PK }, true);
    public static final UniqueKey<TSalescontentRecord> T_SALESCONTENT_FK_SALES_FK_SALESCONTENTTYPE_KEY = Internal.createUniqueKey(TSalescontent.T_SALESCONTENT, DSL.name("t_salescontent_fk_sales_fk_salescontenttype_key"), new TableField[] { TSalescontent.T_SALESCONTENT.FK_SALES, TSalescontent.T_SALESCONTENT.FK_SALESCONTENTTYPE }, true);
    public static final UniqueKey<TSalescontenttypeRecord> T_SALESCONTENTTYPE_PKEY = Internal.createUniqueKey(TSalescontenttype.T_SALESCONTENTTYPE, DSL.name("t_salescontenttype_pkey"), new TableField[] { TSalescontenttype.T_SALESCONTENTTYPE.NAME }, true);
    public static final UniqueKey<TSalesprofileRecord> T_SALESPROFILE_FK_CAMP_FK_PROFILE_KEY = Internal.createUniqueKey(TSalesprofile.T_SALESPROFILE, DSL.name("t_salesprofile_fk_camp_fk_profile_key"), new TableField[] { TSalesprofile.T_SALESPROFILE.FK_CAMP, TSalesprofile.T_SALESPROFILE.FK_PROFILE }, true);
    public static final UniqueKey<TSalesprofileRecord> T_SALESPROFILE_PKEY = Internal.createUniqueKey(TSalesprofile.T_SALESPROFILE, DSL.name("t_salesprofile_pkey"), new TableField[] { TSalesprofile.T_SALESPROFILE.PK }, true);

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<TCampRecord, TDocumentRecord> T_CAMP__T_CAMP_FK_DOCUMENT_FKEY = Internal.createForeignKey(TCamp.T_CAMP, DSL.name("t_camp_fk_document_fkey"), new TableField[] { TCamp.T_CAMP.FK_DOCUMENT }, Keys.T_DOCUMENT_PKEY, new TableField[] { TDocument.T_DOCUMENT.PK }, true);
    public static final ForeignKey<TCampRecord, TLocationRecord> T_CAMP__T_CAMP_FK_LOCATION_FKEY = Internal.createForeignKey(TCamp.T_CAMP, DSL.name("t_camp_fk_location_fkey"), new TableField[] { TCamp.T_CAMP.FK_LOCATION }, Keys.T_LOCATION_PKEY, new TableField[] { TLocation.T_LOCATION.PK }, true);
    public static final ForeignKey<TCampRecord, TProfileRecord> T_CAMP__T_CAMP_FK_PROFILE_FKEY = Internal.createForeignKey(TCamp.T_CAMP, DSL.name("t_camp_fk_profile_fkey"), new TableField[] { TCamp.T_CAMP.FK_PROFILE }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final ForeignKey<TCampdocumentRecord, TCampRecord> T_CAMPDOCUMENT__T_CAMPDOCUMENT_FK_CAMP_FKEY = Internal.createForeignKey(TCampdocument.T_CAMPDOCUMENT, DSL.name("t_campdocument_fk_camp_fkey"), new TableField[] { TCampdocument.T_CAMPDOCUMENT.FK_CAMP }, Keys.T_CAMP_PKEY, new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final ForeignKey<TCampdocumentRecord, TDocumentRecord> T_CAMPDOCUMENT__T_CAMPDOCUMENT_FK_DOCUMENT_FKEY = Internal.createForeignKey(TCampdocument.T_CAMPDOCUMENT, DSL.name("t_campdocument_fk_document_fkey"), new TableField[] { TCampdocument.T_CAMPDOCUMENT.FK_DOCUMENT }, Keys.T_DOCUMENT_PKEY, new TableField[] { TDocument.T_DOCUMENT.PK }, true);
    public static final ForeignKey<TCampprofileRecord, TCampRecord> T_CAMPPROFILE__T_CAMPPROFILE_FK_CAMP_FKEY = Internal.createForeignKey(TCampprofile.T_CAMPPROFILE, DSL.name("t_campprofile_fk_camp_fkey"), new TableField[] { TCampprofile.T_CAMPPROFILE.FK_CAMP }, Keys.T_CAMP_PKEY, new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final ForeignKey<TCampprofileRecord, TProfileRecord> T_CAMPPROFILE__T_CAMPPROFILE_FK_PROFILE_FKEY = Internal.createForeignKey(TCampprofile.T_CAMPPROFILE, DSL.name("t_campprofile_fk_profile_fkey"), new TableField[] { TCampprofile.T_CAMPPROFILE.FK_PROFILE }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final ForeignKey<TDocumentroleRecord, TDocumentRecord> T_DOCUMENTROLE__T_DOCUMENTROLE_FK_DOCUMENT_FKEY = Internal.createForeignKey(TDocumentrole.T_DOCUMENTROLE, DSL.name("t_documentrole_fk_document_fkey"), new TableField[] { TDocumentrole.T_DOCUMENTROLE.FK_DOCUMENT }, Keys.T_DOCUMENT_PKEY, new TableField[] { TDocument.T_DOCUMENT.PK }, true);
    public static final ForeignKey<TLocationRecord, TDocumentRecord> T_LOCATION__T_LOCATION_FK_DOCUMENT_FKEY = Internal.createForeignKey(TLocation.T_LOCATION, DSL.name("t_location_fk_document_fkey"), new TableField[] { TLocation.T_LOCATION.FK_DOCUMENT }, Keys.T_DOCUMENT_PKEY, new TableField[] { TDocument.T_DOCUMENT.PK }, true);
    public static final ForeignKey<TPersonRecord, TCampRecord> T_PERSON__T_PERSON_FK_CAMP_FKEY = Internal.createForeignKey(TPerson.T_PERSON, DSL.name("t_person_fk_camp_fkey"), new TableField[] { TPerson.T_PERSON.FK_CAMP }, Keys.T_CAMP_PKEY, new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final ForeignKey<TPersonRecord, TProfileRecord> T_PERSON__T_PERSON_FK_PROFILE_FKEY = Internal.createForeignKey(TPerson.T_PERSON, DSL.name("t_person_fk_profile_fkey"), new TableField[] { TPerson.T_PERSON.FK_PROFILE }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final ForeignKey<TPersonRecord, TProfileRecord> T_PERSON__T_PERSON_FK_REGISTRATOR_FKEY = Internal.createForeignKey(TPerson.T_PERSON, DSL.name("t_person_fk_registrator_fkey"), new TableField[] { TPerson.T_PERSON.FK_REGISTRATOR }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final ForeignKey<TPersondocumentRecord, TPersonRecord> T_PERSONDOCUMENT__T_PERSONDOCUMENT_FK_PERSON_FKEY = Internal.createForeignKey(TPersondocument.T_PERSONDOCUMENT, DSL.name("t_persondocument_fk_person_fkey"), new TableField[] { TPersondocument.T_PERSONDOCUMENT.FK_PERSON }, Keys.T_PERSON_PKEY, new TableField[] { TPerson.T_PERSON.PK }, true);
    public static final ForeignKey<TProfileroleRecord, TProfileRecord> T_PROFILEROLE__T_PROFILEROLE_FK_PROFILE_FKEY = Internal.createForeignKey(TProfilerole.T_PROFILEROLE, DSL.name("t_profilerole_fk_profile_fkey"), new TableField[] { TProfilerole.T_PROFILEROLE.FK_PROFILE }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
    public static final ForeignKey<TSalesRecord, TCampRecord> T_SALES__T_SALES_FK_CAMP_FKEY = Internal.createForeignKey(TSales.T_SALES, DSL.name("t_sales_fk_camp_fkey"), new TableField[] { TSales.T_SALES.FK_CAMP }, Keys.T_CAMP_PKEY, new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final ForeignKey<TSalesprofileRecord, TCampRecord> T_SALESPROFILE__T_SALESPROFILE_FK_CAMP_FKEY = Internal.createForeignKey(TSalesprofile.T_SALESPROFILE, DSL.name("t_salesprofile_fk_camp_fkey"), new TableField[] { TSalesprofile.T_SALESPROFILE.FK_CAMP }, Keys.T_CAMP_PKEY, new TableField[] { TCamp.T_CAMP.PK }, true);
    public static final ForeignKey<TSalesprofileRecord, TProfileRecord> T_SALESPROFILE__T_SALESPROFILE_FK_PROFILE_FKEY = Internal.createForeignKey(TSalesprofile.T_SALESPROFILE, DSL.name("t_salesprofile_fk_profile_fkey"), new TableField[] { TSalesprofile.T_SALESPROFILE.FK_PROFILE }, Keys.T_PROFILE_PKEY, new TableField[] { TProfile.T_PROFILE.PK }, true);
}
