/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.records;


import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;
import de.jottyfan.camporganizer.db.jooq.tables.TPerson;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record20;
import org.jooq.Row20;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TPersonRecord extends UpdatableRecordImpl<TPersonRecord> implements Record20<Integer, String, String, String, String, String, String, LocalDate, EnumCamprole, String, Integer, Integer, Boolean, LocalDateTime, EnumSex, Integer, BigDecimal, String, Boolean, BigDecimal> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>camp.t_person.pk</code>.
     */
    public TPersonRecord setPk(Integer value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.pk</code>.
     */
    public Integer getPk() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>camp.t_person.forename</code>.
     */
    public TPersonRecord setForename(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.forename</code>.
     */
    public String getForename() {
        return (String) get(1);
    }

    /**
     * Setter for <code>camp.t_person.surname</code>.
     */
    public TPersonRecord setSurname(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.surname</code>.
     */
    public String getSurname() {
        return (String) get(2);
    }

    /**
     * Setter for <code>camp.t_person.street</code>.
     */
    public TPersonRecord setStreet(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.street</code>.
     */
    public String getStreet() {
        return (String) get(3);
    }

    /**
     * Setter for <code>camp.t_person.zip</code>.
     */
    public TPersonRecord setZip(String value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.zip</code>.
     */
    public String getZip() {
        return (String) get(4);
    }

    /**
     * Setter for <code>camp.t_person.city</code>.
     */
    public TPersonRecord setCity(String value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.city</code>.
     */
    public String getCity() {
        return (String) get(5);
    }

    /**
     * Setter for <code>camp.t_person.phone</code>.
     */
    public TPersonRecord setPhone(String value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.phone</code>.
     */
    public String getPhone() {
        return (String) get(6);
    }

    /**
     * Setter for <code>camp.t_person.birthdate</code>.
     */
    public TPersonRecord setBirthdate(LocalDate value) {
        set(7, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.birthdate</code>.
     */
    public LocalDate getBirthdate() {
        return (LocalDate) get(7);
    }

    /**
     * Setter for <code>camp.t_person.camprole</code>.
     */
    public TPersonRecord setCamprole(EnumCamprole value) {
        set(8, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.camprole</code>.
     */
    public EnumCamprole getCamprole() {
        return (EnumCamprole) get(8);
    }

    /**
     * Setter for <code>camp.t_person.email</code>.
     */
    public TPersonRecord setEmail(String value) {
        set(9, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.email</code>.
     */
    public String getEmail() {
        return (String) get(9);
    }

    /**
     * Setter for <code>camp.t_person.fk_camp</code>.
     */
    public TPersonRecord setFkCamp(Integer value) {
        set(10, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.fk_camp</code>.
     */
    public Integer getFkCamp() {
        return (Integer) get(10);
    }

    /**
     * Setter for <code>camp.t_person.fk_profile</code>.
     */
    public TPersonRecord setFkProfile(Integer value) {
        set(11, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.fk_profile</code>.
     */
    public Integer getFkProfile() {
        return (Integer) get(11);
    }

    /**
     * Setter for <code>camp.t_person.accept</code>.
     */
    public TPersonRecord setAccept(Boolean value) {
        set(12, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.accept</code>.
     */
    public Boolean getAccept() {
        return (Boolean) get(12);
    }

    /**
     * Setter for <code>camp.t_person.created</code>.
     */
    public TPersonRecord setCreated(LocalDateTime value) {
        set(13, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.created</code>.
     */
    public LocalDateTime getCreated() {
        return (LocalDateTime) get(13);
    }

    /**
     * Setter for <code>camp.t_person.sex</code>.
     */
    public TPersonRecord setSex(EnumSex value) {
        set(14, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.sex</code>.
     */
    public EnumSex getSex() {
        return (EnumSex) get(14);
    }

    /**
     * Setter for <code>camp.t_person.fk_registrator</code>.
     */
    public TPersonRecord setFkRegistrator(Integer value) {
        set(15, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.fk_registrator</code>.
     */
    public Integer getFkRegistrator() {
        return (Integer) get(15);
    }

    /**
     * Setter for <code>camp.t_person.paid</code>.
     */
    public TPersonRecord setPaid(BigDecimal value) {
        set(16, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.paid</code>.
     */
    public BigDecimal getPaid() {
        return (BigDecimal) get(16);
    }

    /**
     * Setter for <code>camp.t_person.comment</code>.
     */
    public TPersonRecord setComment(String value) {
        set(17, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.comment</code>.
     */
    public String getComment() {
        return (String) get(17);
    }

    /**
     * Setter for <code>camp.t_person.consent_catalog_photo</code>.
     */
    public TPersonRecord setConsentCatalogPhoto(Boolean value) {
        set(18, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.consent_catalog_photo</code>.
     */
    public Boolean getConsentCatalogPhoto() {
        return (Boolean) get(18);
    }

    /**
     * Setter for <code>camp.t_person.required_price</code>.
     */
    public TPersonRecord setRequiredPrice(BigDecimal value) {
        set(19, value);
        return this;
    }

    /**
     * Getter for <code>camp.t_person.required_price</code>.
     */
    public BigDecimal getRequiredPrice() {
        return (BigDecimal) get(19);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record20 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row20<Integer, String, String, String, String, String, String, LocalDate, EnumCamprole, String, Integer, Integer, Boolean, LocalDateTime, EnumSex, Integer, BigDecimal, String, Boolean, BigDecimal> fieldsRow() {
        return (Row20) super.fieldsRow();
    }

    @Override
    public Row20<Integer, String, String, String, String, String, String, LocalDate, EnumCamprole, String, Integer, Integer, Boolean, LocalDateTime, EnumSex, Integer, BigDecimal, String, Boolean, BigDecimal> valuesRow() {
        return (Row20) super.valuesRow();
    }

    @Override
    public Field<Integer> field1() {
        return TPerson.T_PERSON.PK;
    }

    @Override
    public Field<String> field2() {
        return TPerson.T_PERSON.FORENAME;
    }

    @Override
    public Field<String> field3() {
        return TPerson.T_PERSON.SURNAME;
    }

    @Override
    public Field<String> field4() {
        return TPerson.T_PERSON.STREET;
    }

    @Override
    public Field<String> field5() {
        return TPerson.T_PERSON.ZIP;
    }

    @Override
    public Field<String> field6() {
        return TPerson.T_PERSON.CITY;
    }

    @Override
    public Field<String> field7() {
        return TPerson.T_PERSON.PHONE;
    }

    @Override
    public Field<LocalDate> field8() {
        return TPerson.T_PERSON.BIRTHDATE;
    }

    @Override
    public Field<EnumCamprole> field9() {
        return TPerson.T_PERSON.CAMPROLE;
    }

    @Override
    public Field<String> field10() {
        return TPerson.T_PERSON.EMAIL;
    }

    @Override
    public Field<Integer> field11() {
        return TPerson.T_PERSON.FK_CAMP;
    }

    @Override
    public Field<Integer> field12() {
        return TPerson.T_PERSON.FK_PROFILE;
    }

    @Override
    public Field<Boolean> field13() {
        return TPerson.T_PERSON.ACCEPT;
    }

    @Override
    public Field<LocalDateTime> field14() {
        return TPerson.T_PERSON.CREATED;
    }

    @Override
    public Field<EnumSex> field15() {
        return TPerson.T_PERSON.SEX;
    }

    @Override
    public Field<Integer> field16() {
        return TPerson.T_PERSON.FK_REGISTRATOR;
    }

    @Override
    public Field<BigDecimal> field17() {
        return TPerson.T_PERSON.PAID;
    }

    @Override
    public Field<String> field18() {
        return TPerson.T_PERSON.COMMENT;
    }

    @Override
    public Field<Boolean> field19() {
        return TPerson.T_PERSON.CONSENT_CATALOG_PHOTO;
    }

    @Override
    public Field<BigDecimal> field20() {
        return TPerson.T_PERSON.REQUIRED_PRICE;
    }

    @Override
    public Integer component1() {
        return getPk();
    }

    @Override
    public String component2() {
        return getForename();
    }

    @Override
    public String component3() {
        return getSurname();
    }

    @Override
    public String component4() {
        return getStreet();
    }

    @Override
    public String component5() {
        return getZip();
    }

    @Override
    public String component6() {
        return getCity();
    }

    @Override
    public String component7() {
        return getPhone();
    }

    @Override
    public LocalDate component8() {
        return getBirthdate();
    }

    @Override
    public EnumCamprole component9() {
        return getCamprole();
    }

    @Override
    public String component10() {
        return getEmail();
    }

    @Override
    public Integer component11() {
        return getFkCamp();
    }

    @Override
    public Integer component12() {
        return getFkProfile();
    }

    @Override
    public Boolean component13() {
        return getAccept();
    }

    @Override
    public LocalDateTime component14() {
        return getCreated();
    }

    @Override
    public EnumSex component15() {
        return getSex();
    }

    @Override
    public Integer component16() {
        return getFkRegistrator();
    }

    @Override
    public BigDecimal component17() {
        return getPaid();
    }

    @Override
    public String component18() {
        return getComment();
    }

    @Override
    public Boolean component19() {
        return getConsentCatalogPhoto();
    }

    @Override
    public BigDecimal component20() {
        return getRequiredPrice();
    }

    @Override
    public Integer value1() {
        return getPk();
    }

    @Override
    public String value2() {
        return getForename();
    }

    @Override
    public String value3() {
        return getSurname();
    }

    @Override
    public String value4() {
        return getStreet();
    }

    @Override
    public String value5() {
        return getZip();
    }

    @Override
    public String value6() {
        return getCity();
    }

    @Override
    public String value7() {
        return getPhone();
    }

    @Override
    public LocalDate value8() {
        return getBirthdate();
    }

    @Override
    public EnumCamprole value9() {
        return getCamprole();
    }

    @Override
    public String value10() {
        return getEmail();
    }

    @Override
    public Integer value11() {
        return getFkCamp();
    }

    @Override
    public Integer value12() {
        return getFkProfile();
    }

    @Override
    public Boolean value13() {
        return getAccept();
    }

    @Override
    public LocalDateTime value14() {
        return getCreated();
    }

    @Override
    public EnumSex value15() {
        return getSex();
    }

    @Override
    public Integer value16() {
        return getFkRegistrator();
    }

    @Override
    public BigDecimal value17() {
        return getPaid();
    }

    @Override
    public String value18() {
        return getComment();
    }

    @Override
    public Boolean value19() {
        return getConsentCatalogPhoto();
    }

    @Override
    public BigDecimal value20() {
        return getRequiredPrice();
    }

    @Override
    public TPersonRecord value1(Integer value) {
        setPk(value);
        return this;
    }

    @Override
    public TPersonRecord value2(String value) {
        setForename(value);
        return this;
    }

    @Override
    public TPersonRecord value3(String value) {
        setSurname(value);
        return this;
    }

    @Override
    public TPersonRecord value4(String value) {
        setStreet(value);
        return this;
    }

    @Override
    public TPersonRecord value5(String value) {
        setZip(value);
        return this;
    }

    @Override
    public TPersonRecord value6(String value) {
        setCity(value);
        return this;
    }

    @Override
    public TPersonRecord value7(String value) {
        setPhone(value);
        return this;
    }

    @Override
    public TPersonRecord value8(LocalDate value) {
        setBirthdate(value);
        return this;
    }

    @Override
    public TPersonRecord value9(EnumCamprole value) {
        setCamprole(value);
        return this;
    }

    @Override
    public TPersonRecord value10(String value) {
        setEmail(value);
        return this;
    }

    @Override
    public TPersonRecord value11(Integer value) {
        setFkCamp(value);
        return this;
    }

    @Override
    public TPersonRecord value12(Integer value) {
        setFkProfile(value);
        return this;
    }

    @Override
    public TPersonRecord value13(Boolean value) {
        setAccept(value);
        return this;
    }

    @Override
    public TPersonRecord value14(LocalDateTime value) {
        setCreated(value);
        return this;
    }

    @Override
    public TPersonRecord value15(EnumSex value) {
        setSex(value);
        return this;
    }

    @Override
    public TPersonRecord value16(Integer value) {
        setFkRegistrator(value);
        return this;
    }

    @Override
    public TPersonRecord value17(BigDecimal value) {
        setPaid(value);
        return this;
    }

    @Override
    public TPersonRecord value18(String value) {
        setComment(value);
        return this;
    }

    @Override
    public TPersonRecord value19(Boolean value) {
        setConsentCatalogPhoto(value);
        return this;
    }

    @Override
    public TPersonRecord value20(BigDecimal value) {
        setRequiredPrice(value);
        return this;
    }

    @Override
    public TPersonRecord values(Integer value1, String value2, String value3, String value4, String value5, String value6, String value7, LocalDate value8, EnumCamprole value9, String value10, Integer value11, Integer value12, Boolean value13, LocalDateTime value14, EnumSex value15, Integer value16, BigDecimal value17, String value18, Boolean value19, BigDecimal value20) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        value14(value14);
        value15(value15);
        value16(value16);
        value17(value17);
        value18(value18);
        value19(value19);
        value20(value20);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached TPersonRecord
     */
    public TPersonRecord() {
        super(TPerson.T_PERSON);
    }

    /**
     * Create a detached, initialised TPersonRecord
     */
    public TPersonRecord(Integer pk, String forename, String surname, String street, String zip, String city, String phone, LocalDate birthdate, EnumCamprole camprole, String email, Integer fkCamp, Integer fkProfile, Boolean accept, LocalDateTime created, EnumSex sex, Integer fkRegistrator, BigDecimal paid, String comment, Boolean consentCatalogPhoto, BigDecimal requiredPrice) {
        super(TPerson.T_PERSON);

        setPk(pk);
        setForename(forename);
        setSurname(surname);
        setStreet(street);
        setZip(zip);
        setCity(city);
        setPhone(phone);
        setBirthdate(birthdate);
        setCamprole(camprole);
        setEmail(email);
        setFkCamp(fkCamp);
        setFkProfile(fkProfile);
        setAccept(accept);
        setCreated(created);
        setSex(sex);
        setFkRegistrator(fkRegistrator);
        setPaid(paid);
        setComment(comment);
        setConsentCatalogPhoto(consentCatalogPhoto);
        setRequiredPrice(requiredPrice);
    }

    /**
     * Create a detached, initialised TPersonRecord
     */
    public TPersonRecord(de.jottyfan.camporganizer.db.jooq.tables.pojos.TPerson value) {
        super(TPerson.T_PERSON);

        if (value != null) {
            setPk(value.getPk());
            setForename(value.getForename());
            setSurname(value.getSurname());
            setStreet(value.getStreet());
            setZip(value.getZip());
            setCity(value.getCity());
            setPhone(value.getPhone());
            setBirthdate(value.getBirthdate());
            setCamprole(value.getCamprole());
            setEmail(value.getEmail());
            setFkCamp(value.getFkCamp());
            setFkProfile(value.getFkProfile());
            setAccept(value.getAccept());
            setCreated(value.getCreated());
            setSex(value.getSex());
            setFkRegistrator(value.getFkRegistrator());
            setPaid(value.getPaid());
            setComment(value.getComment());
            setConsentCatalogPhoto(value.getConsentCatalogPhoto());
            setRequiredPrice(value.getRequiredPrice());
        }
    }
}
