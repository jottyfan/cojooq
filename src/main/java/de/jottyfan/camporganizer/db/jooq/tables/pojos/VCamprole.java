/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables.pojos;


import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;

import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VCamprole implements Serializable {

    private static final long serialVersionUID = 1L;

    private final EnumCamprole name;

    public VCamprole(VCamprole value) {
        this.name = value.name;
    }

    public VCamprole(
        EnumCamprole name
    ) {
        this.name = name;
    }

    /**
     * Getter for <code>camp.v_camprole.name</code>.
     */
    public EnumCamprole getName() {
        return this.name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VCamprole (");

        sb.append(name);

        sb.append(")");
        return sb.toString();
    }
}
