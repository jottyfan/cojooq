/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.enums;


import de.jottyfan.camporganizer.db.jooq.Camp;

import org.jooq.Catalog;
import org.jooq.EnumType;
import org.jooq.Schema;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public enum EnumFiletype implements EnumType {

    pdf("pdf"),

    png("png"),

    jpg("jpg");

    private final String literal;

    private EnumFiletype(String literal) {
        this.literal = literal;
    }

    @Override
    public Catalog getCatalog() {
        return getSchema().getCatalog();
    }

    @Override
    public Schema getSchema() {
        return Camp.CAMP;
    }

    @Override
    public String getName() {
        return "enum_filetype";
    }

    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Lookup a value of this EnumType by its literal
     */
    public static EnumFiletype lookupLiteral(String literal) {
        return EnumType.lookupLiteral(EnumFiletype.class, literal);
    }
}
