/*
 * This file is generated by jOOQ.
 */
package de.jottyfan.camporganizer.db.jooq.tables;


import de.jottyfan.camporganizer.db.jooq.Camp;
import de.jottyfan.camporganizer.db.jooq.Keys;
import de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole;
import de.jottyfan.camporganizer.db.jooq.enums.EnumSex;
import de.jottyfan.camporganizer.db.jooq.tables.records.TPersonRecord;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row20;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TPerson extends TableImpl<TPersonRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>camp.t_person</code>
     */
    public static final TPerson T_PERSON = new TPerson();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TPersonRecord> getRecordType() {
        return TPersonRecord.class;
    }

    /**
     * The column <code>camp.t_person.pk</code>.
     */
    public final TableField<TPersonRecord, Integer> PK = createField(DSL.name("pk"), SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>camp.t_person.forename</code>.
     */
    public final TableField<TPersonRecord, String> FORENAME = createField(DSL.name("forename"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.surname</code>.
     */
    public final TableField<TPersonRecord, String> SURNAME = createField(DSL.name("surname"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.street</code>.
     */
    public final TableField<TPersonRecord, String> STREET = createField(DSL.name("street"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.zip</code>.
     */
    public final TableField<TPersonRecord, String> ZIP = createField(DSL.name("zip"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.city</code>.
     */
    public final TableField<TPersonRecord, String> CITY = createField(DSL.name("city"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.phone</code>.
     */
    public final TableField<TPersonRecord, String> PHONE = createField(DSL.name("phone"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.birthdate</code>.
     */
    public final TableField<TPersonRecord, LocalDate> BIRTHDATE = createField(DSL.name("birthdate"), SQLDataType.LOCALDATE, this, "");

    /**
     * The column <code>camp.t_person.camprole</code>.
     */
    public final TableField<TPersonRecord, EnumCamprole> CAMPROLE = createField(DSL.name("camprole"), SQLDataType.VARCHAR.asEnumDataType(de.jottyfan.camporganizer.db.jooq.enums.EnumCamprole.class), this, "");

    /**
     * The column <code>camp.t_person.email</code>.
     */
    public final TableField<TPersonRecord, String> EMAIL = createField(DSL.name("email"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.fk_camp</code>.
     */
    public final TableField<TPersonRecord, Integer> FK_CAMP = createField(DSL.name("fk_camp"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>camp.t_person.fk_profile</code>.
     */
    public final TableField<TPersonRecord, Integer> FK_PROFILE = createField(DSL.name("fk_profile"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>camp.t_person.accept</code>.
     */
    public final TableField<TPersonRecord, Boolean> ACCEPT = createField(DSL.name("accept"), SQLDataType.BOOLEAN, this, "");

    /**
     * The column <code>camp.t_person.created</code>.
     */
    public final TableField<TPersonRecord, LocalDateTime> CREATED = createField(DSL.name("created"), SQLDataType.LOCALDATETIME(6).defaultValue(DSL.field("now()", SQLDataType.LOCALDATETIME)), this, "");

    /**
     * The column <code>camp.t_person.sex</code>.
     */
    public final TableField<TPersonRecord, EnumSex> SEX = createField(DSL.name("sex"), SQLDataType.VARCHAR.asEnumDataType(de.jottyfan.camporganizer.db.jooq.enums.EnumSex.class), this, "");

    /**
     * The column <code>camp.t_person.fk_registrator</code>.
     */
    public final TableField<TPersonRecord, Integer> FK_REGISTRATOR = createField(DSL.name("fk_registrator"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>camp.t_person.paid</code>.
     */
    public final TableField<TPersonRecord, BigDecimal> PAID = createField(DSL.name("paid"), SQLDataType.NUMERIC(7, 2), this, "");

    /**
     * The column <code>camp.t_person.comment</code>.
     */
    public final TableField<TPersonRecord, String> COMMENT = createField(DSL.name("comment"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>camp.t_person.consent_catalog_photo</code>.
     */
    public final TableField<TPersonRecord, Boolean> CONSENT_CATALOG_PHOTO = createField(DSL.name("consent_catalog_photo"), SQLDataType.BOOLEAN.nullable(false).defaultValue(DSL.field("false", SQLDataType.BOOLEAN)), this, "");

    /**
     * The column <code>camp.t_person.required_price</code>.
     */
    public final TableField<TPersonRecord, BigDecimal> REQUIRED_PRICE = createField(DSL.name("required_price"), SQLDataType.NUMERIC(7, 2), this, "");

    private TPerson(Name alias, Table<TPersonRecord> aliased) {
        this(alias, aliased, null);
    }

    private TPerson(Name alias, Table<TPersonRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>camp.t_person</code> table reference
     */
    public TPerson(String alias) {
        this(DSL.name(alias), T_PERSON);
    }

    /**
     * Create an aliased <code>camp.t_person</code> table reference
     */
    public TPerson(Name alias) {
        this(alias, T_PERSON);
    }

    /**
     * Create a <code>camp.t_person</code> table reference
     */
    public TPerson() {
        this(DSL.name("t_person"), null);
    }

    public <O extends Record> TPerson(Table<O> child, ForeignKey<O, TPersonRecord> key) {
        super(child, key, T_PERSON);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : Camp.CAMP;
    }

    @Override
    public Identity<TPersonRecord, Integer> getIdentity() {
        return (Identity<TPersonRecord, Integer>) super.getIdentity();
    }

    @Override
    public UniqueKey<TPersonRecord> getPrimaryKey() {
        return Keys.T_PERSON_PKEY;
    }

    @Override
    public List<UniqueKey<TPersonRecord>> getUniqueKeys() {
        return Arrays.asList(Keys.UK_PERSON);
    }

    @Override
    public List<ForeignKey<TPersonRecord, ?>> getReferences() {
        return Arrays.asList(Keys.T_PERSON__T_PERSON_FK_CAMP_FKEY, Keys.T_PERSON__T_PERSON_FK_PROFILE_FKEY, Keys.T_PERSON__T_PERSON_FK_REGISTRATOR_FKEY);
    }

    private transient TCamp _tCamp;
    private transient TProfile _tPersonFkProfileFkey;
    private transient TProfile _tPersonFkRegistratorFkey;

    /**
     * Get the implicit join path to the <code>camp.t_camp</code> table.
     */
    public TCamp tCamp() {
        if (_tCamp == null)
            _tCamp = new TCamp(this, Keys.T_PERSON__T_PERSON_FK_CAMP_FKEY);

        return _tCamp;
    }

    /**
     * Get the implicit join path to the <code>camp.t_profile</code> table, via
     * the <code>t_person_fk_profile_fkey</code> key.
     */
    public TProfile tPersonFkProfileFkey() {
        if (_tPersonFkProfileFkey == null)
            _tPersonFkProfileFkey = new TProfile(this, Keys.T_PERSON__T_PERSON_FK_PROFILE_FKEY);

        return _tPersonFkProfileFkey;
    }

    /**
     * Get the implicit join path to the <code>camp.t_profile</code> table, via
     * the <code>t_person_fk_registrator_fkey</code> key.
     */
    public TProfile tPersonFkRegistratorFkey() {
        if (_tPersonFkRegistratorFkey == null)
            _tPersonFkRegistratorFkey = new TProfile(this, Keys.T_PERSON__T_PERSON_FK_REGISTRATOR_FKEY);

        return _tPersonFkRegistratorFkey;
    }

    @Override
    public TPerson as(String alias) {
        return new TPerson(DSL.name(alias), this);
    }

    @Override
    public TPerson as(Name alias) {
        return new TPerson(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TPerson rename(String name) {
        return new TPerson(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TPerson rename(Name name) {
        return new TPerson(name, null);
    }

    // -------------------------------------------------------------------------
    // Row20 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row20<Integer, String, String, String, String, String, String, LocalDate, EnumCamprole, String, Integer, Integer, Boolean, LocalDateTime, EnumSex, Integer, BigDecimal, String, Boolean, BigDecimal> fieldsRow() {
        return (Row20) super.fieldsRow();
    }
}
